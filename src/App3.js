import React from "react";
import "./App3.css";
import Header from "./components/header/Header.js";
import { Counter, CounterContext } from "./counter.js";
import { themes } from "./theme.js";
class App3 extends React.Component {
  state = {
    count: Counter.counter,
    //   setCounter:
    theme: themes.dark,
  };
  changeTheme = () => {
    this.setState({
      count: this.state.count + 1,
      theme: this.state.theme == themes.dark ? themes.light : themes.dark,
    });
  };
  render() {
    return (
      <>
        <CounterContext.Provider value={this.state.theme}>
          <Header />
          <h1 className="App-body">12345</h1>
          <button onClick={this.changeTheme}>Click</button>
        </CounterContext.Provider>
      </>
    );
  }
}

export default App3;
