import "./App2.css";
let button = (text) => {
  return (
    <>
      <button className="button">{text}</button>
    </>
  );
};
let card = (text) => {
  return (
    <>
      <div className="card">
        <p>
          January 18th 2023, The Breakfast Club, Virtual Book Reading of Swan
          Isle, 7PM - 8PM EST
        </p>
        {button(text)}
      </div>
    </>
  );
};
let comment = () => {
  return (
    <>
      <div className="comment">
        <div>
          <svg
            preserveAspectRatio="xMidYMid meet"
            data-bbox="30.5 43.999 139 112"
            viewBox="30.5 43.999 139 112"
            height="36"
            width="36"
            xmlns="http://www.w3.org/2000/svg"
            data-type="shape"
            role="presentation"
            // aria-hidden="true"
            // aria-labelledby="svgcid-43qgi8jn38gf"
          >
            {/* <title id="svgcid-43qgi8jn11gf"></title> */}
            <g>
              <path d="M84.463 44.235c2.428 4.024 4.786 8.047 7.352 12.348-1.179.694-2.358 1.457-3.537 2.081-9.294 5.411-18.242 11.169-25.525 19.355-7.422 8.325-11.028 18.037-12.069 29.344.971 0 1.942-.069 2.913 0 5.757.416 11.653.139 17.202 1.457 13.109 3.052 18.103 13.111 17.202 25.39-.902 11.724-10.612 21.159-22.126 21.713-8.462.416-16.577-.763-23.236-6.59-6.034-5.272-8.74-12.418-10.474-19.979-4.995-22.337 1.456-41.414 16.577-57.994 7.63-8.325 16.508-15.123 26.08-21.02 3.121-1.942 6.104-4.024 9.225-6.105h.416z"></path>
              <path d="M162.148 44c2.428 4.024 4.786 8.047 7.352 12.348-1.179.694-2.358 1.457-3.537 2.081-9.294 5.411-18.242 11.169-25.525 19.355-7.422 8.325-11.028 18.037-12.069 29.344.971 0 1.942-.069 2.913 0 5.757.416 11.653.139 17.202 1.457 13.109 3.052 18.103 13.111 17.202 25.39-.902 11.724-10.612 21.159-22.126 21.713-8.462.416-16.577-.763-23.236-6.59-6.034-5.272-8.74-12.418-10.474-19.979-4.994-22.338 1.457-41.415 16.577-57.995 7.63-8.325 16.508-15.123 26.08-21.02 3.121-1.942 6.104-4.024 9.225-6.105l.416.001z"></path>
            </g>
          </svg>
        </div>
        <p>
          I'm a paragraph. Click here to add your own text and edit me. It’s
          easy. Just click “Edit Text” or double click me to add your own
          content and make changes to the font. I'm a great place for you to
          tell a story and let your users know a little more about you.
        </p>
        <p>'Swan Isle' is Griffith's best writing yet” The Times Book Review</p>
        <hr></hr>
      </div>
    </>
  );
};
function App() {
  return (
    <>
      <header>
        <div className="navi">
          <nav>
            <h1>E. Muhammad</h1>
            <ul className="navbar">
              <li>
                <a href="apperesnce">Apperence</a>
              </li>
              <li>
                <a href="books">Books</a>
              </li>
              <li>
                <a href="news">News</a>
              </li>
              <li>
                <a href="about">About</a>
              </li>
              <li>
                <a href="contact">Contact</a>
              </li>
            </ul>
          </nav>
          <div>
            <img
              src="https://static.wixstatic.com/media/ad420a_ca05081d4ece492fb6589c81baee1091~mv2.jpg/v1/crop/x_1416,y_0,w_3091,h_3414/fill/w_238,h_263,al_c,q_80,usm_0.66_1.00_0.01/GettyImages-11967148821.webp"
              alt="headimage"
            ></img>
            <div>
              <h1>Kayla Griffith</h1>
              <p>Award Winning Author</p>
            </div>
          </div>
        </div>
      </header>
      <section id="heroSection">
        <div className="overlay">
          <div className="container">
            <div className="heroCard">
              <div className="side">
                <p>
                  New Release
                  <span></span>
                </p>
                <div className="heroCardBody">
                  <h1>
                    The Swan Isle <br></br> (2023)
                  </h1>
                  <p>
                    I'm a paragraph. Click here to add your own text and edit
                    me. It's easy. Just click “Edit Text” or double click me to
                    add your own content and make changes to the font. I'm a
                    great place for you to tell a story and let your users know
                    a little more about you.
                  </p>
                  <h3>Order Now</h3>
                  <div className="buttons">
                    {button("Amazon")}
                    {button("Google")}
                    {button("ibooks")}
                  </div>
                </div>
              </div>
              <div className="side">
                <img
                  src="https://static.wixstatic.com/media/ad420a_ac6bbfd46dbe4c83a234221d29d67791~mv2.png/v1/fill/w_241,h_339,al_c,q_85,usm_0.66_1.00_0.01/Screen%20Shot%202021-02-07%20at%209_24_47.webp"
                  alt="image"
                  width={"50%"}
                ></img>
              </div>
              <div></div>
            </div>
          </div>
        </div>
        <div className="">
          <div className="rewiews">
            <h1>Praise & Reviews</h1>
            <div className="comments">
              {comment()}
              {comment()}
              {comment()}
            </div>
          </div>
        </div>
      </section>
      <section className="posts">
        <div className="container">
          <h1>See Upcoming Appearances</h1>
          <p>
            I'm a paragraph. Click here to add your own <br></br> text and edit
            me. It's easy.
          </p>
          {card("Join")}
          {card("Join")}
          {card("Join")}
        </div>

        <div className="container">
          <div className="aboutme">
            <img
              src="https://static.wixstatic.com/media/ad420a_3b0ac18b8f8c44e6968cf137c044b3b4~mv2.jpg/v1/crop/x_1678,y_0,w_3414,h_3414/fill/w_111,h_111,al_c,q_80,usm_0.66_1.00_0.01/GettyImages-11967148791.webp"
              alt="img"
            ></img>
            <h1>About Kayla Griffith</h1>
            <p>
              About Kayla Griffith I'm a paragraph. Click here to add your own
              text and edit me. It’s easy. Just click “Edit Text” or double
              click me to add your own content and make changes to the font. I’m
              a great place for you to tell a story and let your users know a
              little more about you.
            </p>
            {button("Read More")}
          </div>
        </div>
      </section>
      <footer>
        <p>© 2023 by K.Griffith. Proudly created with Muhammad</p>
      </footer>
    </>
  );
}

export default App;
