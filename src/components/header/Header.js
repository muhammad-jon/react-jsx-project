import React from "react";
import "./Header.css";
import { CounterContext } from "../../counter";
import { ThemesContext } from "../../theme.js";

const Header = () => {
  return (
    <ThemesContext.Consumer>
      {(value) => {
        return (
          <h1
            className="header"
            style={({ backgroundColor: value.background }, { color: value })}
          >
            {value}
          </h1>
        );
      }}
    </ThemesContext.Consumer>
  );
};

export default Header;
